import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";

// import ServicesContext from "./context/ServicesContext";
// import UserCartService from "./services/UserCartService";
import { StateProvider } from "./services/stateProvider";

import App from "./App";

import "./index.css";

document.title = "E-shop";

// const userCartService = new UserCartService();

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      {/* <ServicesContext.Provider value={{ userCartService }}> */}
      <StateProvider>
        <App />
      </StateProvider>
      {/* </ServicesContext.Provider> */}
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);
