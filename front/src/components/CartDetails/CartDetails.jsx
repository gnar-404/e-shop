import React from "react";

import defaultImg from "../../images/default1.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";

const CartDetails = ({ userProducts, currentUser, deleteFromCart }) => {
  if (userProducts.length > 0) {
    return (
      <div className="container mt-5">
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Image</th>
              <th scope="col">Title</th>
              <th scope="col"></th>
            </tr>
          </thead>
          {userProducts.map(({ id, image, title }) => (
            <tbody key={id}>
              <tr>
                <td>
                  <img
                    src={image.substring(0, 5) === "https" ? image : defaultImg}
                    className="card-img-top"
                    style={{ width: 50, height: 50 }}
                    alt={title}
                  />
                </td>
                <td>{title}</td>
                <td>
                  <button className="btn btn-danger" onClick={deleteFromCart} value={id}>
                    Remove from cart <FontAwesomeIcon icon={faTrashAlt} />
                  </button>
                </td>
              </tr>
            </tbody>
          ))}
        </table>
      </div>
    );
  } else {
    return (
      <div className="d-flex justify-content-center">
        {currentUser !== undefined ? <h1>No products selected</h1> : <h1>Please login</h1>}
      </div>
    );
  }
};

export default CartDetails;
