import React from "react";
import { BiShoppingBag } from "react-icons/bi";

const CartSummary = ({ cartCount }) => (
  <div>
    <h2>
      <BiShoppingBag />
      {cartCount}
    </h2>
  </div>
);

export default CartSummary;
