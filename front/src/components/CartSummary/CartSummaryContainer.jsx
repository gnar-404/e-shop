import React, { useContext, useEffect, useState } from "react";
import axios from "axios";
import baseUrl from "../../AppConfig";
// import ServicesContext from "../../context/ServicesContext";
import CartSummary from "./CartSummary";
///??????????????????????????????????????????????????????????
import { store } from "../../services/stateProvider";
import { setCartCount } from "../../services/actions";
import { getCartCount, getCurrentUser } from "../../services/selectors";

const CartSummaryContainer = () => {
  // const { userCartService } = useContext(ServicesContext);
  const stateService = useContext(store);
  const currentUser = getCurrentUser(stateService);
  // const setUserName = setUserName(stateService);
  const cartCount = getCartCount(stateService);
  const { dispatch } = stateService;

  // const [currentUser, setCurrentUser] = useState(userCartService.getCurrentUser());
  // const [getCurrentUser, setUserName] = useState(userCartService.getCurrentUser());
  // const [cartCount, setCartCount] = useState(userCartService.getCartCount());

  // userCartService.updateCurrentUser = () => setCurrentUser(userCartService.getCurrentUser());
  // userCartService.updateCartCount = () => setCartCount(userCartService.getCartCount());

  useEffect(
    () => {
      if (currentUser !== undefined) {
        axios
          .get(`${baseUrl}/api/users/${currentUser}/cart-products`)
          .then((response) => {
            // userCartService.setCartCount(response.data.length());
            dispatch(setCartCount(response.data.length));
            // userCartService.updateCartCount();
          })
          .catch((err) => console.log(err));
      }
    }
    // , [currentUser, cartCount, userCartService] ???????????????????????????? KAS CIA BUVO DARYTA???
  );

  return (
    <div>
      <CartSummary cartCount={cartCount} />
    </div>
  );
};

export default CartSummaryContainer;
