import React from "react";
import { Link } from "react-router-dom";

import defaultImg from "../../images/default1.png";

var styles = {
  img: {
    height: "20rem",
  },
};

const ProductDetails = ({ title, image, description, price, quantity, currentUser, addToCart }) => {
  let imgSrc = image.substring(0, 5) === "https" ? image : defaultImg;
  return (
    <div>
      <div className="media">
        <img className="align-self-start mr-3" src={imgSrc} style={styles.img} alt={defaultImg} />
        <div className="media-body mt-3">
          <h5 className="mt-0">{title}</h5>
          <p>{description}</p>
          <p>Kaina: {price}</p>
          <p>Kiekis: {quantity} vnt.</p>
        </div>
      </div>
      <div className="row ml-5 mt-3">
        {currentUser !== undefined ? (
          <button className="btn btn-success mr-1" onClick={addToCart}>
            Add
          </button>
        ) : (
          <div></div>
        )}
        <Link to={"/"} className="btn btn-info">
          Back
        </Link>
      </div>
    </div>
  );
};

export default ProductDetails;
