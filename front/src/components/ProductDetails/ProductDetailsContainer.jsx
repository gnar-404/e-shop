import React, { useContext, useEffect, useState } from "react";
import axios from "axios";
import baseUrl from "../../AppConfig";

import { store } from "../../services/stateProvider";
import { setCartCount } from "../../services/actions";
import { getCurrentUser } from "../../services/selectors";

import ProductDetails from "./ProductDetails";

const ProductDetailsContainer = (props) => {
  const stateService = useContext(store);
  const { dispatch } = stateService;

  const currentUser = getCurrentUser(stateService);

  const [product, setProduct] = useState(null);

  useEffect(() => {
    axios
      .get(`${baseUrl}/api/products/${props.match.params.id}`)
      .then((res) => setProduct(res.data))
      .catch((err) => console.log(err));
  }, [props.match.params.id]);

  const addToCart = () => {
    axios
      .post(`${baseUrl}/api/users/${currentUser}/cart-products`, {
        id: product.id,
        image: product.image || "",
        title: product.title,
      })
      .then(() => {
        axios.get(`${baseUrl}/api/users/${currentUser}/cart-products`).then((res) => {
          dispatch(setCartCount(res.data.length));
        });
        // javoj post metodas galetu grazint userio produktus
        // .then(res => {
        //     userCartService.setCartCount(res.data.length);
        //     userCartService.updateCartCount();
      })
      .catch((err) => console.log(err));
  };

  if (product !== null) {
    const { id, image, title, ...otherProps } = product;
    console.log(id);
    return (
      <div className="container">
        <div key={id}>
          <ProductDetails
            id={id}
            currentUser={currentUser}
            image={image}
            addToCart={addToCart}
            title={title}
            {...otherProps}
          />
        </div>
      </div>
    );
  } else {
    return (
      <div className="d-flex justify-content-center">
        <div className="spinner-border" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
  }
};

export default ProductDetailsContainer;
