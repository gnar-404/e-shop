import React, { useContext } from "react";
import axios from "axios";
import { useHistory } from "react-router";
import baseUrl from "../../AppConfig";

import { store } from "../../services/stateProvider";
import { setUserName, clearUserName, setCartCount } from "../../services/actions";
import { getCurrentUser } from "../../services/selectors";

import LoginComponent from "./LoginComponent";

const LoginContainer = () => {
  const stateService = useContext(store);
  const { dispatch } = stateService;

  const history = useHistory();

  const handleLogin = (e) => {
    e.preventDefault();
    const name = e.target.username.value;

    dispatch(setUserName(name));

    axios
      .post(`${baseUrl}/api/users/`, {
        username: name,
      })
      .then(() =>
        axios
          .get(`${baseUrl}/api/users/${name}/cart-products`)
          .then((res) => {
            dispatch(setCartCount(res.data.length));
          })
          .catch((err) => console.log(err))
      )
      .then(() => history.push("/"));
  };

  const handleLogout = (e) => {
    e.preventDefault();

    dispatch(clearUserName());
    dispatch(setCartCount(0));

    history.push("/");
  };

  return (
    <LoginComponent handleLogin={handleLogin} handleLogout={handleLogout} currentUser={getCurrentUser(stateService)} />
  );
};

export default LoginContainer;
