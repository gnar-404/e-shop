import React, { useEffect, useState } from "react";
import axios from "axios";
import baseUrl from "../../AppConfig";
import ProductCard from "./ProductCard";

const ProductListComponent = () => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    axios
      .get(`${baseUrl}/api/products`)
      .then((response) => setProducts(response.data))
      .catch((err) => console.log(err));
  }, []);

  if (products.length > 0) {
    return (
      <div className="container">
        <div className="row">
          {products.map(({ id, ...otherProps }) => {
            return <ProductCard id={id} key={id} {...otherProps} />;
          })}
        </div>
      </div>
    );
  } else {
    return (
      <div className="d-flex justify-content-center">
        <div>
          <p>Loading...</p>
        </div>
      </div>
    );
  }
};

export default ProductListComponent;
