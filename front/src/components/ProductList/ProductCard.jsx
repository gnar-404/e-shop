import React from "react";
import { Link } from "react-router-dom";

import defaultImg from "../../images/default1.png";

var styles = {
  thumbnail: {
    maxWidth: "280px",
    textAlign: "center",
    marginLeft: "auto",
    marginRight: "auto",
  },
  // image: { width: "100%", height: "250px", display: "block" },
};

const ProductCard = ({ id, title, image, description, price, quantity }) => {
  let imgSrc = image.substring(0, 5) === "https" ? image : defaultImg;
  return (
    <div className="container col-12 col-sm-12 col-md-6 col-lg-4 mb-4">
      <div className="m-3 card px-2 pt-3" style={styles.thumbnail}>
        <img src={imgSrc} style={styles.image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">
            {title}, id: {id}
          </h5>
          <p className="card-text">Description: {description}</p>
          <div>
            <p>Kaina: {price} €</p>
          </div>
          {/* <div>
            <p>Kiekis: {quantity}</p>
          </div> */}
          <Link to={`/products/${id}`} className="btn btn-info">
            Details here
          </Link>
        </div>
      </div>
    </div>
  );
};

export default ProductCard;
