import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import CartSummaryContainer from "../CartSummary/CartSummaryContainer";
import LoginContainer from "../Login/LoginContainer";
import { store } from "../../services/stateProvider";
import { getCurrentUser } from "../../services/selectors";

const Navigation = () => {
  const stateService = useContext(store);
  const currentUser = getCurrentUser(stateService);

  return (
    <nav className="navbar navbar-light navbar-expand-md" style={{ backgroundColor: "#EECF9C" }}>
      <div className="container">
        <ul className="nav navbar-nav">
          <NavLink className="nav-link" exact to="/">
            Home
          </NavLink>
          <NavLink className="nav-link" to={`/admin/products`}>
            Admin
          </NavLink>
          <LoginContainer />
        </ul>
        <ul className="nav navbar-nav">
          <NavLink className="nav-link" to={`/users/${currentUser}/cart-products`}>
            <CartSummaryContainer />
          </NavLink>
          <div>Vartotojas: {currentUser}</div>
        </ul>
      </div>
    </nav>
  );
};

export default Navigation;
