export function setUserName(name) {
  return {
    type: "set_username",
    userName: name,
  };
}

export function clearUserName() {
  return {
    type: "clear_username",
  };
}

export function setCartCount(cartCount) {
  return {
    type: "set_cartCount",
    cartCount,
  };
}
