export const getCurrentUser = (service) => service.state.userName;
export const getCartCount = (service) => service.state.cartCount;
