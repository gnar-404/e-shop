import React, { createContext, useReducer } from "react";

const initialState = {
  userName: undefined,
  cartCount: 0,
};

const store = createContext(initialState);
const { Provider } = store;

const StateProvider = ({ children }) => {
  const [state, dispatch] = useReducer((state, action) => {
    switch (action.type) {
      case "set_username":
        return { ...state, userName: action.userName };

      case "clear_username":
        return { ...state, userName: undefined };

      case "set_cartCount":
        return { ...state, cartCount: action.cartCount };
      //Mano naujas case!!!!!!!!!!!!!!!1CartSummaryContaineriui
      // case "set_cartCount":
      //   return { ...state, cartCount: action.cartCount };

      default:
        throw new Error();
    }
  }, initialState);

  return <Provider value={{ state, dispatch }}>{children}</Provider>;
};

export { store, StateProvider };
