## Embedded Tomcat
### App 
```
mvn org.codehaus.cargo:cargo-maven2-plugin:1.7.7:run -Dcargo.maven.containerId=tomcat9x -Dcargo.servlet.port=8082 -Dcargo.maven.containerUrl=https://repo1.maven.org/maven2/org/apache/tomcat/tomcat/9.0.40/tomcat-9.0.40.zip
```
```http://localhost:8082/Shopify/```
### Swagger
```http://localhost:8082/Shopify/swagger-ui/```
### H2
```http://localhost:8082/Shopify/console```




## Spring-boot
In order to run using spring-boot use the following command 
```mvn spring-boot:run```
### App 
Port configuration in pom.xml 
```mvn spring-boot:run```
```http://localhost:8083/```
### Swagger
```http://localhost:8083/swagger-ui/```
### Debug 
Default debug port 5005 ( pom.xml )
### H2
```http://localhost:8082/console```
### Reference structure
https://medium.com/the-resonant-web/spring-boot-2-0-project-structure-and-best-practices-part-2-7137bdcba7d3