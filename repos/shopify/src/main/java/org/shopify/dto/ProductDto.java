package org.shopify.dto;

import org.shopify.model.Product;

import java.math.BigDecimal;

public class ProductDto {
    private Long id;

    private String title;

    private String image;

    private String description;

    private BigDecimal price;

    private Long quantity;

    public ProductDto(Product product){
        this.id = product.getId();
        this.title = product.getTitle();
        this.image = product.getImage();
        this.description = product.getDescription();
        this.price = product.getPrice();
        this.quantity = product.getQuantity();
    }

    public ProductDto() {
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
