package org.shopify.dto;

import org.shopify.model.CartProduct;

public class CartProductDto {
    private Long id;
    private Long quantity;

    public CartProductDto(CartProduct product){
        this.id = product.getId();
        this.quantity = product.getQuantity();
    }

    public CartProductDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
