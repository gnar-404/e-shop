package org.shopify.repository;

import org.shopify.model.Cart;
import org.shopify.model.CartProduct;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<Cart, Long> {
    Cart findByUsername ( String username);
//    void save(Cart cart);
}
