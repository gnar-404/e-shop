package org.shopify.model;

import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Entity
@Table
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String username;
//    @OneToMany(cascade = CascadeType.ALL)
//    @JoinTable(name = "cart_item_mapping",
//            joinColumns = {@JoinColumn(name = "item_id", referencedColumnName = "id")},
//            inverseJoinColumns = {@JoinColumn(name = "cart_id", referencedColumnName = "id")})
//    @MapKey(name = "id")


    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "cart_id")
    private Set<CartProduct> products = new HashSet<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<CartProduct> getProducts() {
        return products;
    }

    public void setProducts(Set<CartProduct> products) {
        this.products = products;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
