package org.shopify.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.shopify.dto.ProductDto;
import org.shopify.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value="product")
public class ProductController {
   @Autowired
   private ProductService productService;

    @RequestMapping(method = RequestMethod.GET, value = "/api/products")
    @ApiOperation(value="Get products",notes ="Returns products")
    public List<ProductDto> getProducts(){
        return productService.getProducts();
    }

    @ApiOperation(value = "Get single product by id", notes="Returns a single product by id")
    @RequestMapping(path="/api/products/{product_id}",method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<Object> getProduct(@PathVariable final Long product_id){
        try{
            return new ResponseEntity<Object>(productService.getProduct(product_id), HttpStatus.OK);
        }
        catch (Exception err){
            return new ResponseEntity<Object>( null, HttpStatus.NOT_FOUND);
        }
    }



    @RequestMapping(method = RequestMethod.POST, value = "/api/products")
    @ApiOperation(value = "Add product", notes="Adds product")
    public ResponseEntity<Object> createProduct(
            @ApiParam(value = "", required = true)
            @RequestBody ProductDto product
    )
            {
                productService.addProduct(product);
                return ResponseEntity.accepted().build();
    }

    @ApiOperation(value="Update product", notes = "Updates product by id")
    @RequestMapping(method=RequestMethod.PUT, value = "/api/products/{id}")
    public ResponseEntity<Object> updateProduct(
            @ApiParam(value = "", required = true)
            @PathVariable Long id,
            @RequestBody ProductDto productDto
    )
    {
        productService.updateProduct(id, productDto);
        return ResponseEntity.accepted().build();
    }

    @ApiOperation(value = "Delete product", notes= "Deletes product by id")
    @RequestMapping(method = RequestMethod.DELETE, value = "/api/products/{id}")
    public ResponseEntity<Object> deleteProduct(
            @ApiParam(value = "", required = true)
            @PathVariable Long id){

            productService.removeProduct(id);
            return ResponseEntity.accepted().build();
    }


    public void setProductService(ProductService productService) {
        this.productService = productService;
    }
}
