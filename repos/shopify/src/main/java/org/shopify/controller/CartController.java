package org.shopify.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.shopify.dto.CartProductDto;
import org.shopify.dto.ProductDto;
import org.shopify.service.CartProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(value="cart")
public class CartController {
    @Autowired
    private CartProductService cartProductService;

    @RequestMapping(method = RequestMethod.POST, value = "/api/users/{username}/cart-products")
    @ApiOperation(value="Add product to cart",notes ="Adds product to user cart")
    public ResponseEntity<Object> addCartProduct(
            @ApiParam(value = "", required = true)
            @PathVariable String username,
            @RequestBody CartProductDto cartProductDto
    ){

        cartProductService.addCartProduct(username, cartProductDto);
        return ResponseEntity.ok().build();

    }

    @RequestMapping(method = RequestMethod.GET, value = "/api/users/{username}/cart-products")
    @ApiOperation(value = "return products", notes = "returns products per username")
    public ResponseEntity<Object> getCartProducts(
            @ApiParam(value = "",required = true)
            @PathVariable String username
    ){
        return new ResponseEntity<Object>( cartProductService.getCartProducts(username), HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.DELETE, value = "/api/users/{username}/cart-products")
    @ApiOperation(value = "delete user cart", notes = "deletes cart")
    public ResponseEntity<Object> removeCart(
            @ApiParam(value = "",required = true)
            @PathVariable String username
    ){
        cartProductService.deleteCart(username);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/api/users/{username}/cart-products")
    @ApiOperation(value = "update product", notes = "updates product in cart by username")
    public ResponseEntity<Object> updateProduct(
            @ApiParam(value = "",required = true)
            @PathVariable String username,
            @RequestBody CartProductDto product
    ){
        cartProductService.updateProduct(username,product.getId() ,product);
        return ResponseEntity.ok().build();
    }

    public void setCartProductService(CartProductService cartProductService) {
        this.cartProductService = cartProductService;
    }
}
