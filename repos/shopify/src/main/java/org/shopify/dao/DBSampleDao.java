//package org.petz.dao;
//
//import org.petz.model.Pet;
//import org.springframework.stereotype.Repository;
//
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import java.util.List;
//
//@Repository
//public class DBPetDao implements PetDao{
//    @PersistenceContext
//    private EntityManager entityManager;
//
//    public DBPetDao(EntityManager entityManager) {
//        this.entityManager = entityManager;
//    }
//
//    @Override
//    public List<Pet> getPets() {
//        return entityManager.createQuery("SELECT pet FROM Pet pet", Pet.class).getResultList();
//    }
//}
