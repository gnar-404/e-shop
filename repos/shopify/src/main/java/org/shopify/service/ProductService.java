package org.shopify.service;

import org.shopify.dto.ProductDto;
import org.shopify.model.Product;
import org.shopify.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;


    @Transactional(readOnly=true)
    public List<ProductDto> getProducts(){
        List<Product> products = productRepository.findAll();

        List<ProductDto> productList = products.stream().map(product -> new ProductDto(product)).collect(Collectors.toList());

        return productList;
    }

    @Transactional(readOnly=true)
    public ProductDto getProduct(Long id) {
        Product product = productRepository.getOne(id);
        return new ProductDto(product);
    }


    @Transactional
    public void addProduct(ProductDto productDto) {
        Product product = new Product();
        product.setTitle(productDto.getTitle());
        product.setDescription(productDto.getDescription());
        product.setImage(productDto.getImage());
        product.setPrice(productDto.getPrice());
        product.setQuantity(productDto.getQuantity());

        productRepository.save(product);

    }

    @Transactional
    public void updateProduct(Long id, ProductDto productDto){
        Optional<Product> product = Optional.ofNullable(productRepository.getOne(id));
        if(product.isEmpty()){

        } else {
            product.get().setTitle(productDto.getTitle());
            product.get().setQuantity(productDto.getQuantity());
            product.get().setPrice(productDto.getPrice());
            product.get().setImage(productDto.getImage());
            product.get().setDescription(productDto.getDescription());

            productRepository.save(product.get());
        }
    }


    @Transactional
    public void removeProduct(Long id) {
        productRepository.deleteById(id);
    }


    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

}
