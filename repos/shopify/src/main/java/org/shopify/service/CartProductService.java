package org.shopify.service;

import org.shopify.dto.CartProductDto;
import org.shopify.model.Cart;
import org.shopify.model.CartProduct;

import org.shopify.model.Product;
import org.shopify.repository.CartRepository;
import org.shopify.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CartProductService {
    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private ProductRepository productRepository;

    @Transactional
    public void addCartProduct(String username, CartProductDto productDto){
        CartProduct cartProduct = new CartProduct();
        cartProduct.setQuantity(productDto.getQuantity());

        Product product = productRepository.getOne(productDto.getId());
        product.setCartProduct(cartProduct);

        cartProduct.setProduct(product);
        Cart cart = null;
        try{
            cart = cartRepository.findByUsername(username);
        } catch (EntityNotFoundException e){}

        if(cart == null){
            cart = new Cart();
            cart.setUsername(username);
        }

        cart.getProducts().add(cartProduct);
        cartRepository.save(cart);
    }

    @Transactional(readOnly = true)
    public Set<CartProductDto> getCartProducts(String username) {
        try{
            return cartRepository.findByUsername(username).getProducts().stream().map(product -> new CartProductDto(product)).collect(Collectors.toSet());
        } catch (Exception e) {
            return new HashSet<>();
        }
    }

    @Transactional
    public void deleteCart(String username) {
        cartRepository.delete(cartRepository.findByUsername(username));
    }

    @Transactional
    public void updateProduct(String username, Long id, CartProductDto product) {
        Cart cart = cartRepository.findByUsername(username);
        Set<CartProduct> products = cart.getProducts();

        CartProduct xxx = null;
        for (CartProduct item:products ){
            if(item.getId()==product.getId()){
                xxx=item;
            }
        }
        xxx.setQuantity(product.getQuantity());

        cartRepository.save(cart);
    }

    public void setCartRepository(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }
}
